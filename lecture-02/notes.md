$ HIST 200
% The Book in Early China
% Wed, 15 Sept, 2021

# Early China

Books are also ephemera and isn't copied

## Writing Methods

In Early China all books had to be handwritten (due to the limited technology)

- Bamboo/Wood strip (>1250 BCE)
    - We know that bamboo/wood was in use before 1250 BCE
    - Was rolled into scrolls
        - Was not uncommon for the elite to be buried with this
    - Would later be bound into codex (pseudo-books)
- Silk writing
    - Silk industry was already well established
        - Led into easy transition into use in writing
    - Has several advantages over bamboo
        - Easier to store
        - Less bulky
        - Harder to break
        - Can bend
    - Is very expensive
        - Is almost exclusively used by the elite
- Stele (stone)
    - Used since at least the Han Dynasty (200s BCE-200s CE)
    - Have funerary/commemorative uses
        - Once irrelevant they are moved into a stele "forest"
    - Also used for core societal texts
    - Difficult to write on
        - Scribes needed to be highly skilled
- Paper
    - In use since at least (105 CE)
        - Most likely invented earlier
    - Important trade good
        - Can even be used to pay taxes
    - Gradually spread across the world
    - Taxing process to make and is proprietary
        - Making process kept secret by paper making

## Writing Storage

During this time China was also always finding new ways to manage their writing
surfaces (besides scrolls)

- Sutra Folded Binding
    - ![sutra](sutra-binding.jpg)
- Butterfly Binding
    - ![butterfly](butterfly-binding.png)
- Ink-Squeeze Rubbings
    - Combination of paper and stele
        - Stele is used to emboss letters onto paper
    - Intellectual basis for xylographic (woodcut) printing
    - ![ink](ink-squeeze-rubbing.jpg)

# Printing in China

- Woodblock printing (xylographic printing)
    - Started seeing use in 200s CE, but took off 700s-900s CE
        - late Tang to Early Song Dynasties
    - Dominated most printing
        - Resulted in creation of printing "businesses"
        - Highly skilled districts of printing formed
    - Spread to neighboring societies
    - Used for religious and secular purposes
- Moveable Woodcut Printing
    - Can be used to make a large amount of copies
    - Did not see as much use as woodblock printing
        - Possibly due to complication of Chinese language
        - Does not see use outside of China
    - 1040 CE: porcelain movable type (Bi Sheng)
    - 1298 CE: re-introduction of wooden moveable type (Wang Zhen)
