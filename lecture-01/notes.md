$ HIST 200
% History of Writing
% Mon, 13 Sept, 2021

Books are a means of transmitting a culture's traditions.
Books were rare and expensive in ancient times

# Ancient Mesopotamia

- Cuneiform was a mode of writing in Ancient Mesopotamia
    - Around ~3100 BCE
    - Ephemera(l)
- Most texts were bureaucratic in nature

![Example of a cuneiform](cuneiform.jpg)

- Emergence of text of cultural significance in ~2000 BCE
    - Accumulation and standardization of knowledge over time
    - Copying of texts began
        - Cylinder stamps (form of printing) w/ embossed text

# Mediterranean Part 1

Ancient Egypt were the major proponents of document replication

- Papyrus sheets/scrolls were made by the Egyptians
    - Ease of use facilitated the ability to write
        - Manuscript could easily last thousands of years
    - Major trading resource (coveted)
    - Were in use from ~2000 BCE
- Book of the Dead is the most famous Egyptian work
    - Full of spells/incantations to help the dead in the afterlife

# Ancient Greeks

Ancient Greeks also discovered papyrus later on (900-500 BCE)

- Great Library of Alexandria was created to collect knowledge
    - Was located at a massive shipping centre
        - Finished construction around 200 BCE
    - Collected works from people coming back from expeditions
