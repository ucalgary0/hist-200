[Click here to see index of other files](index.md)

# Description

This project contains all the notes that I am creating for HIST-200. Anyone
is encouraged to create pull requests to change anything wrong or clone the
repository for their own private viewing

# Structure

[index.md](index.md) is contains links to all the other 'important' files
in other directories. This is most commonly the notes for that
lecture/reading. Just click on the file you want to go to in
[index.md](index.md) and gitlab will automatically take you there

## Directory Structure

The repo makes heavy use of directories for organization

- lecture-xx
    - Contains notes from each lecture that I actually care to write notes in
- readings
    - Contains notes from each reading
    - Each reading is sorted into subdirectories based on the reading's
      name

# Compilation

The notes are taken in markdown and then compiled into pdf with pandoc.
Depending on the need I'll embed some latex here and there

For anyone wishing to contribute *(lol)* here's the command I use
*(I'll probably make a template soon)*

`pandoc --toc --number-section -V geometry:margin=2cm -V fontsize=12pt`
