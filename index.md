# Readings

- [Origins of the Modern World](readings/origins-of-modern-world/notes.md)
    - [PDF Version](readings/origins-of-modern-world/notes.pdf)
- [Drucker - History of the Book](readings/history-of-the-book/notes.md)
    - [PDF Version](readings/history-of-the-book/notes.pdf)
- [Dittmar - Info Tech and Economic Change](readings/dittmar/notes.md)
    - [PDF Version](readings/dittmar/notes.pdf)

# Lecture

- [Lecture-01](lecture-01/notes.md)
    - [PDF Version](lecture-01/notes.pdf)
- [Lecture-02](lecture-02/notes.md)
    - [PDF Version](lecture-02/notes.pdf)
