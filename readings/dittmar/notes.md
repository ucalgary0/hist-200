% Info Tech and Economic Change
% The Impact of the Printing Press
% Mon, September 27, 2021

# Introduction

## Preamble

> The printing press was the great innovation in early modern information
technology, but economists have found no macroeconomic evidence of its
impact.  This article exploits city-level data. Between 1500 and 1600,
European cities where printing presses were established in the 1400s grew
60% faster than otherwise similar cities. Cities that adopted printing in
the 1400s had no prior advantage, and the association between adoption and
subsequent growth was not due to printers choosing auspicious locations.
These findings are supported by regressions that exploit distance from
Mainz, Germany—the birthplace of printing—as an instrument for adoption.

- 50 years after it's invention (1450) the price of books fell by 2/3
    - No evidence of productivity gains
    - But research suggests print media transformed...
        - The way ideas were disseminated
        - The accumulation of human capital
        - Evolution of business practices

## Body

- Between 1450-1500 printing tech diffused into *"concentric circles"* as
  printers established press in other cities
    - Distance from Mainz was strongly associated with early adoption
        - Not associated with city growth before diffusion
        - Large relationship between adoption and growth **after** diffusion
    - Printing press fostered valuable knowledge in commerce
        - Played role in development of numeracy
        - Emergence of business education
        - Adoption of innovations in bookkeeping/accounting
    - Port cities benefited from innovations in commercial practice 
        - Add new dimension to consider effects of printing press

# The Mechanism

- Cities that adopted print benefited from localized spillovers in human
  capital accumulation, tech change, and forward/backward linkages
    - Contributed to city growth by exerting pressure on returns to labor
        - Localized by high transport costs associated with intercity trade
    - Made cities culturally dynamic and attracted migrants
        - Important because city growth was driven by immigration
        - Commercial success drove migration, driving commercial success
    - Fueled advances in arithmetic
        - Allowed for creation of textbooks/manuals
        - Commercial arithmetics were of particular importance
- Gave opportunities for less privileged to obtain education
- Development of new bourgeois competences
    - Urban middle classes principal purchasers of book
        - For *"Men who needed to read, write, calculate in order to manage
          their businesses and conduct civic affairs"*
- Benefits didn't translate to neighbouring regions due to cost to
  transport print
    - "City printing" accounted for large share of production and were even
      less widely traded
    - Often reprinted rather than transported

<!-- End of page 9 -->
