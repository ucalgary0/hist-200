% History of the Book
% Chapter 5
% Fri, September 17, 2021

<!-- Textwidth = 75 -->
# Precursor to Printing

The idea of printing multiple copies of a text for a literate audience is a
modern concept, with the first to start likely being the Chinese (w/
woodblocks)

However, Johann Gutenberg revolutionized the scale and scope of printing in
the 15th century

- Made mass-printing feasible
    - Would lead to the industrial revolution (~+100 yrs)
    - Debates about access to religious texts split along lines of belief
    - Fostered already increasing literacy
- Spread across the globe quickly
    - Carried throughout Europe, British Isles, Russia, etc
    - Eventually followed colonialization
- Was a moveable type rather than printing press
    - **Do not conflate the creation of the moveable type as being
      responsible for the creation of printing proper and the invention of
      the book**
    - Led to many things integral to society today
        - Standardization of language
        - Stabilization of texts
        - Dissemination of multiple copies of works

# Invention of Printing

## In the Far East

- Paper was invented in China ~105 CE
    - Knowledge of paper production spread across the East
    - By 900 CE paper was readily available in Near East
        - At this time the Islamic Empire was at its peak in power
    - Paper came to the West from the Islamic Empire
- Paper production was differentiated by the use of local materials
    - Early paper has texture that varies due to the fineness of the pulp
      and toughness of the plant fibers

### Discovery of Paper Production

Different nations discovered method to paper production at varying times

|          | EG | MR | ESP | ITA | GER | ENG | PHILLY | Mayan/Aztec |
|----------|:--:|:--:|:---:|:---:|:---:|:---:|:------:|:-----------:|
| 1000 BCE |    |    |     |     |     |     |        |      X      |
| 900 CE   | X  |    |     |     |     |     |        |             |
| ~1200 CE |    | X  |  X  |     |     |     |        |             |
| 1270 CE  |    |    |     |  X  |     |     |        |             |
| 1390 CE  |    |    |     |     |  X  |     |        |             |
| 1494 CE  |    |    |     |     |     |  X  |        |             |
| 1690 CE  |    |    |     |     |     |     |   X    |             |

- Spanish limited the production of paper in colonies
    - Was a way to control population and local traditions
- Mayan/Aztec was different (called amate)
    - Made from the inner bark of a fig tree

### Engagement with Literacy

Literacy naturally brings about controversy. With tensions between scholars
and political powers bringing about tragic events

- China's engagement with politics of literary has a long history
    - Emperor of the Qin Empire
        - Burnt hundreds of books in 213 BCE
        - Buried 400 Confucian scholars
    - Traditions of Buddhism/Confucianism relied on text for study
        - So widespread piracy had become a problem by 11th century
    - Large labour force was producing commercial books by 12-13th century

Chinese engagement with literacy became foundational to their society

- Chinese traditions keep their distinction even today
    - Copies of canonical works kept the intellectual legacy intact
- Emperor Sik Quanshu ordered 100s of scholars to copy books from known
  corpus
    - About 13,000 books were collected, with 3500 to be copied
        - Rest of books were deemed "contrary to 'official thinking' and
          destroyed"
    - Emperor had multiple copies of classics across...
        - Confucianism
        - History
        - Geography
        - Agriculture
        - Military Strategy
        - Law
        - Medicine
        - Records of folklife
        - Science
        - Literature
        - Catalogues of plant and animals
        - Daoist and Buddhist philosophy
    - Emperor would create new libraries to house all the books

## Printing in Europe

- Religious images were some of the first printing in Europe
- Playing cards were the most commonly printed item
    - Card printing became a major industry
        - Cards were so popular they even became subject to prohibition
        - Guilds regulated pigments, applied colors, prepared paper, etc
    - **Example of printing being popular before Gutenberg's inventions**
- Books printed from blocks preceded those from metal type by a few years
    - Block books were essentially graphic novels
        - The story is straightforward and the conflicts clear
        - Graphical nature gave artists much more freedom
    - Staple of publishing world
    - Found themselves in images, initial letters, borders, etc

### Gutenberg and his Inventions

Of course, Gutenberg's innovations are still a technical invention and a
conceptual leap

- Design of metal type for printing required that letters be cast
  efficiently *(multiple at once, perfectly rectangular)*
    - Process is costly, difficult, and cannot be automated *(until 19th
      century)*
    - Print shop became a place of skill and segmented production
        - All tasks had to be tightly coordinated
        - Led into modern printing practices
- Gutenberg invented a mold that had variable width
    - Allowed for multiple letters to be printed
        - Allowed the printing of text to become commonplace
    - The main invention by Gutenberg
- Gutenberg's first book was a 42-line bible
    - ![Picture of the Biblia Latina](biblia-latina.jpg)
    - The first Western book printed with moveable type

**The printing press was a modification of existing tech, not a radical
invention.** They were made of wood, not to be converted to metal until the
beginning of the 19th century *(coincided w/ increase in size)*

- Gutenberg's shop remained practically unchanged for more than 300 years
    - Only industrialization advanced printing beyond its earliest tech

### Outside Gutenberg

For a time this process was exclusive to Gutenberg, but it could not be
controlled and printing began to spread rapidly

- In new environments, local writing styles were printed onto metal (fonts)
    - Printers would take inspiration from scribal predecessors
- Demand for books outstripped the ability to produce them
    - Readership grew across academics, merchants, and civil servants
- Each type of book had their own format
    - All features of manuscript books were adopted for printed works
    - Works of literature, law, and textbooks were in constant use
    - Scientific works were more difficult to print 
- Within a few decades hand-finishing quickly diminished
    - Still used to create custom designs (borders, etc)
- People began reading to learn rather than learning to read in the 15th
  century
    - At this time non-religious texts outnumbered religious texts
        - Trivial things like decorum, cooking, and politics were made

# Spread of Print

> By the end of the 15th century, more than 29,000 titles had appeared in
print and though many books were in issued in runs of 200-300 copies, this
still represented an exponential increase in the number of books available
for readers before the invention of printing with movable type. An
estimated 10M books may have been produced in less than 50 years

- Presses sprang up all over to fulfill demand
- More than 200 presses were established in Venice before 1500
    - Venice was attractive due to its nature as a trade port
    - One such press became the first press to case Hebrew type
        - Daniel Bomberg's editions are considered milestone works
        - Type was also cast in 
    - Venice became a new ground for new languages to be pressed
        - Daniel Bomberg would print Hebrew type, and his editions are
          considered milestone works
        - Type was also cast in Arabic, Armenian, Greek, and Latin
        - 3/4 of all **incunabula** were in ancient languages
    - Printers were not organized into guilds, making it possible for
      anyone to set up shop
- Scholar-printers were common, with Aldus Manutius standing out
    - Saw potential to provide convenient, portable, cheap editions
    - Created the pocketbook format
        - Produced a compressed font called *italic*
    - Had a large enterprise
        - Workers lived in a school/sweatshop/boardinghouse hybrid
        - Literacy was essential to working in such an enterprise

## Reception to Printing

> The question of whether print makes texts more stable, whether print
technology is one of fixity or fluidity, cannot be answered without taking
into account the ways that errors, editing, copying, circulation, and use
all factor into the way a text is produced and received

Elizabeth Eisenstein's 1980 publication, *The Printing Press as an Agent of
Change*, made an argument for the revolutionary impact of print

- Suggested that print brought major social effects
    - Patterns of knowledge changed by shift from scarcity to wide
      availability
        - Change from "manuscript culture" to "print culture"
- Criticisms do exist however
    - Literacy requires education, not just access
    - Book perpetuates a deterministic pov
        - A more complex social process is likely at work
    - Printing does not necessarily mean textual stability
    - Editions vary in editorial choices, errors, or omissions
        - Even single editions suffer from changes copy-to-copy in the
          effort to correct errors
- Initial reception to printing was not uniformly positive
    - First printings were labelled devils' works
    - But familiarity replaced fear as the technology proved its uses
- Public took time to adjust to paradigm shift
    - Considerable number of remaindered volumes by 1500
        - Represented a faltering industry from overproduction
    - Books diffused through community through book-fairs

## Improvement in Skill

- Xylographic books continued to be printed, but letterpress books were
  more common
    - The skill of engravers grew rapidly, becoming an art-form
- Advertising started during book-fairs
    - Printers' marks created a brand identity for a publisher's imprint

## Aesthetics of Print

- Aesthetics of metal type/printed books matured only in 16th century
    - Copperplate engraving was invented
        - Intaglio process, plates must be printed separately using a
          different press than letterpress type
        - Allowed for elaborate flourished, arabesques, and font designs
        - Found heavy use in map-making
- Renaissance played large part in maturation
- Printers were scholars
    - **Works embodies humanist principles**
        - Trained in Greek and Latin
        - Committed to classical tradition
        - Wrote major treatises on variety of topics
    - Christopher Plantin remains notable printer
        - Sheer output unparalleled anywhere at this period
- Medieval hand-illumination displaced by elegant metal-bordered
    - New borders could be printed efficiently

### Handwriting

The effects that printing had on literacy also allowed the usage of writing
to become more widespread

- Gentlemen/Noble women knew how to write
- Each literacy tech informs the usage of another, rather than existing in
  isolation

# Conclusion

> Printing in the Far East and in the West made use of certain common
technological inventions, but they also differed in their use of materials
(wood vs. metal) and engagement with a press (essential to European
printing, while Asian printing remained a handcraft). The impact of
printing registers within an already receptive and literate environment,
but the availability of a massive number of new works, in relatively
inexpensive formats, and across an increasingly varied range of topics and
genres, created new reading publics for sacred and secular texts in
classical and modern languages. The rationalization of labor that the print
shop practices embodied would serve as a model ahead for the transformation
of other industries into their increasingly modern form, anticipating some
of the modularity reinforced by 19th- century industrialization. The
aesthetics of metal type came into their own, supplying new designs and
forms of letters that were not entirely dependent on scribal models, though
in the first decades of print, all features of the books and artifacts that
were printed borrowed from manuscript traditions because these were the
models in place, ready to be imitated. By the 16th century, issues of
control and censorship were starting to be felt, and the risks associated
with printing, and books as sites and instruments of contestation, would
alter the stakes of print publication. Book culture also had an impact in
the New World, both for its shaping of missions and attitudes and also for
the way print became integrated into emerging cultures in zones of contact
and (often troubled exchange, as will be evident in the next chapter.
