% HIST 200
% The Origins of the Modern World
% Thur, Sept 09 2021

# Introduction

The human world is composed of social, economic, political, and cultural
*structures*.

The reading analyzes the world in 1400, including...

- The material and natural conditions
    - The agricultural world
    - *"biological old regime"*
- The trading networks that connected the Old World
    - The commercial world

- Most people shared a similar material world
    - Due to farming being the main way by which people ate
    - All agriculturalists shared challenges

# The Biological Old World

Earth's population is an important indicator of humanity's success in creating
the material conditions where the population could increase/decrease

## The Weight of Numbers

- Earth's population was historically always steadily rising with sharp declines
  happening periodically
- The world was overwhelmingly rural
    - The availability of land and nutrients was a constant constraint

|            | 900  | 1350 | 1400  | 1700 | 1800  | 2020 |  2050   |
|------------|:----:|:----:|:-----:|:----:|:-----:|:----:|:-------:|
| Population |      |      | 380 M |      | 950 M | 7 B  | 9-9.5 M |
| Rise/Down  | Rise | Down | Rise  | Down | Rise  | Rise |  Rise   |

## Climate Change

- Climate change was a general cause of the pre-modern population increases
    - Due to the effect it had on food production
    - Cooling/Warming trends lead to population decreases/increases respectively
- Climate change accounts less for population growth since 1700
    - New World resources and industrialization began to ease prior constraints

## Population Density and Civilization

- In 1400 humanity was clustered
    - Lived on 4.25 M miles^2 or 7% of dry land
        - 70% of people still live on the same land
    - Because that land was most suitable for agriculture
    - Consolidated into 15 notable civilizations
- Densest areas were in Eurasian continent
    - China in East, Europe in West, India in South
        - These centres were 70%-80% of world from 1400-1800
        - China = 25-40% of population
        - Europe = 25% of population
        - India = 20% of population
    - All dense populations had countryside farmers and city folk consumers

## Agricultural Revolution

- People learned how to grow from 11000-4500 years ago independently
    - Change from hunter-gather society to sedentary agricultural society
- Created an "agricultural surplus"
    - Rise if social groups that didn't produce their own food
        - Priests, rulers, warriors, and outside raiders
    - Now others could take food (by force or like taxes)
    - Rise in cities and writing
        - People could live separate from main village
- Cities traded heavily for raw material/livestock
    - Rulers tended to distrust trade if goods were strategic
        - Preferred to bring producing region under control
        - Gave rise to empires

## Towns and Cities in 1400

- Number/Size of towns/cities corresponds with a society's wealth
    - 25 largest cities represented 1+% of world's population. Another 9% lived in
      towns/cities from 5000-75000 people
        - Most were in Asia
    - Wealth of world concentrated in Asia
- Peasants held up city-life in taxes, tithes, and rent
- Cities tended to take up nutrients form soil
    - Led to crisis if not replaced

## Nomadic Pastoralists

- Pastoral nomads lived through hunting and following herds
    - Mobility on horses was necessary
    - Not completely self-sufficient
        - Civilizations and nomads had a symbiotic relationship
        - Picked up manufactured goods from cities
        - Traded for horses, meat, honey, etc
    - Were skilled in combat and raided for food
- Were viewed as "barbarians"
    - The nomads were not the only barbarians
        - Chinese classified 2 types, "cooked" (willing) and "raw" (not)
- Weakened civilizations susceptible to nomadic raids, invasion, destruction, or
  conquest
    - Civilizations would also incorporate nomadic warriors into their armies,
      opening them to conquest from within
